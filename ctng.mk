
.PHONY: copy_sysroot_to_out arm_toolchain clean_arm_toolchain

ARM_CXX_RELATIVE_PATH := toolchains/arm/ctng/built/bin/arm-unknown-linux-gnueabi-g++
ARM_SYSROOT := toolchains/arm/ctng/built/arm-unknown-linux-gnueabi/sysroot
CXX := $(BASE_DIR)/$(ARM_CXX_RELATIVE_PATH)

PLATFORM_FIXED_PATH := "$(BASE_DIR)/toolchains/arm/ctng/built/arm-unknown-linux-gnueabi/bin/:$(BASE_DIR)/toolchains/arm/ctng/built/bin/:$(PATH)"

platform_check: arm_toolchain
platform_distclean: distclean_arm_toolchain


copy_sysroot_to_out: $(BUILD_DIR)/lib/ld-linux.so.3
$(BUILD_DIR)/lib/ld-linux.so.3: arm_toolchain
	@echo
	@echo "==== Copying cross-compiled libc to [$(BUILD_DIR)] ===="
	@mkdir -p $(BUILD_DIR) 2>/dev/null
	@cp -a $(ARM_SYSROOT)/* $(BUILD_DIR)/
	@echo

arm_toolchain: arm-ctng-toolchain
arm-ctng-toolchain: crosstool-ng $(ARM_CXX_RELATIVE_PATH)
$(ARM_CXX_RELATIVE_PATH):
	@echo
	@echo "==== Building ARM Toolchain with crosstool-ng ===="
	@cd toolchains/arm && \
                make CTNG_CONFIG=$(CTNG_CONFIG) ctng-toolchain
	@echo

distclean_arm_toolchain: clean_arm_toolchain
	@echo "==== Removing ARM toolchains ===="
	@cd toolchains/arm && make distclean

